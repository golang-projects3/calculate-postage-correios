package main

import (
	"net/http"
	"github.com/gorilla/mux"
	"shipping/controller"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/shipping", controller.Shipping).Methods(http.MethodGet)
	http.ListenAndServe(":5000", router)
}