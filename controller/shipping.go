package controller

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/rogpeppe/go-charset/charset"
	_ "github.com/rogpeppe/go-charset/data"
)

type QueryParams struct {
	zipCodeO string
	zipCodeD string
	weight string
	length string
	height string
	width string
	productValue string
	codeService string 
}

type ShippingCalc struct {
	CServico struct {
		Codigo                string `xml:"Codigo" json:"codigo"`
		Valor                 string `xml:"Valor" json:"valor"`
		PrazoEntrega          string `xml:"PrazoEntrega" json:"prazoEntrega"`
		ValorSemAdicionais    string `xml:"ValorSemAdicionais" json:"valorSemAdicionais"`
		ValorMaoPropria       string `xml:"ValorMaoPropria" json:"valorNaoPropria"`
		ValorAvisoRecebimento string `xml:"ValorAvisoRecebimento" json:"valorAvisoRecebimento"`
		ValorDeclarado        string `xml:"ValorValorDeclarado" json:"valorDeclarado"`
		EntregaDomiciliar     string `xml:"EntregaDomiciliar" json:"entregaDomicializar"`
		EntregaSabado         string `xml:"EntregaSabado" json:"entregaSabado"`
	} `xml:"cServico" json:"frete"`
}

func ammountUrl(params *QueryParams) string {
	const urlToCalcShipping = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=%s&sCepDestino=%s&nVlPeso=%s&nCdFormato=1&nVlComprimento=%s&nVlAltura=%s&nVlLargura=%s&sCdMaoPropria=n&nVlValorDeclarado=%s&sCdAvisoRecebimento=n&nCdServico=%s&nVlDiametro=0&StrRetorno=xml"
	return fmt.Sprintf(urlToCalcShipping, params.zipCodeO, params.zipCodeD, params.weight, params.length, params.height, params.width, params.productValue, params.codeService)
}

func extractParamsQuery(params url.Values) *QueryParams   {
   var queryParams QueryParams
	 
	 queryParams.width = params.Get("width")
	 queryParams.height = params.Get("height")
	 queryParams.zipCodeD = params.Get("zipCodeD")
	 queryParams.codeService = params.Get("codeService")
	 queryParams.productValue = params.Get("productValue")
	 queryParams.zipCodeO = params.Get("zipCodeO")
	 queryParams.length = params.Get("length")
	 queryParams.weight = params.Get("weight")

	 return &queryParams
}

func requestToApi(url string) ([]byte, error) {
	response, error := http.Get(url)

	if error != nil {
		fmt.Println("error", error)
	}
	
	readBody, error := io.ReadAll(response.Body)

	if error != nil {
		fmt.Println("erro ao ler corpo da requisição")
		return nil, error
	}

	return readBody, nil
}

func XmlToJson(xmlByte []byte) ([]byte, error) {
  var shipping ShippingCalc

	reader := bytes.NewReader(xmlByte)
	decoder := xml.NewDecoder(reader)
	decoder.CharsetReader = charset.NewReader
	
	

	error := decoder.Decode(&shipping)



	if error != nil {
		return xmlByte, error
	}

	if error := xml.Unmarshal(xmlByte, &shipping); error != nil {
		fmt.Println(error)
	}

	shippingInJson, err := json.Marshal(shipping)

    if nil != err {
        fmt.Println("Error marshalling to JSON", err)
        return xmlByte, error
    }



	return shippingInJson, nil
}

func Shipping(res http.ResponseWriter, req *http.Request) {

	 _, error := ioutil.ReadAll(req.Body)

	 if error != nil {
		res.Write([]byte("error"))
	 }

	 query := req.URL.Query()

	 paramsQuery := extractParamsQuery(query)


	 stringOfShippingToRequest := ammountUrl(paramsQuery)


	 byteResponse, error := requestToApi(stringOfShippingToRequest)

	 if error != nil {
		fmt.Println("erro interneo")
	 }

	 shipping, error := XmlToJson(byteResponse)

	 if error != nil {
		fmt.Println(error)
	 }


	res.Header().Set("Content-Type", "application/json")
	res.Write([]byte(shipping))
}